```bash
docker build -t iat/app:alpha -f deploy/app/Dockerfile .

docker rm -f $(docker ps -qa)

docker run -td --network dev-network --name mongo mongo:3.7

docker run -td \
    --network dev-network  \
    -p 8888:80  \
    -v /home/paty/Documents/TUM/Application_Project/iff-iat-master/templates:/templates \
    -v /home/paty/Documents/TUM/Application_Project/iff-iat-master/dconfigs.json:/root/configs.json \
    -v /home/paty/Documents/TUM/Application_Project/iff-iat-master/disclaimer.md:/root/disclaimer.md \
    --name app  \
    iat/app:alpha
```


export VERSION=<PUT VERSION HERE>
docker build -t iat/app:$VERSION -f deploy/app/Dockerfile .
docker tag  iat/app:$VERSION iat/app:latest
docker tag  iat/app:$VERSION registry.calsaverini.com:5000/iat/app:$VERSION
docker tag  iat/app:$VERSION registry.calsaverini.com:5000/iat/app:latest
docker push registry.calsaverini.com:5000/iat/app:$VERSION
docker push registry.calsaverini.com:5000/iat/app:latest


nosetests --with-coverage -v --cover-erase --cover-package=backend --cover-html --cover-branches --cover-html-dir=/tmp 

nosetests -v --with-coverage --cover-erase --cover-package=backend
