Data Analysis instructions.

In the command line run:
source analysis_env/bin/activate
jupyter notebook

In Jupyter Notebook open:
Analysis.ipynb
Make sure Jupyter Notebook is using the kernel analysis_env
